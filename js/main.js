function getDocument(address, source, post, destName) {
	if (destName===undefined)
		destName='#content'
	if (post===undefined) //Jeśli destname jest "undefined" ustaw na <div id=#content>
		post=''
	const comm = new XMLHttpRequest() 	//Za pomoca "XMLHttpRequest" tworzymy zmiena comm ktora tworzy zadanie do przegladarki
	comm.onreadystatechange= function(e) {
		console.log('OPERACJA')
		if (comm.readyState === 4) { //comm.readyState jest rowne 4
			if (comm.status === 200) { //comm.status jest rowne 200
				window.history.replaceState('', '', "?site="+source);
				document.querySelector(destName).innerHTML = comm.responseText  //Przechodzenie na inna podstrone nie spowoduje przeladowanie strony tylko odrazu wyswietli podstrone				
				if (source==='kontakt')
					addFormEvents() //jesli source jest rowny kontakt, uzyj addfromevents
			}
		}
	}
	
	comm.open('POST',address)
	comm.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	comm.send(post)
}

function addEvents(){
	document.querySelectorAll(".button").forEach( v => {	
		v.onclick = (e) => {
			getDocument('./sites/'+v.dataset.link+'.html', v.dataset.link)
			//document.location='./sites/'+v.dataset.link+'.html'
			//document.location=v.dataset.link	
		}
	})		
}

function setSite() { //tworzymy funkcje setsite
	const site = document.location.search.substr(1).split('=')[1]
	getDocument(`./sites/${site}.html`, site)
}

function addFormEvents() { 	//tworzymy funkcje addformevents
	document.querySelector("#form button").onclick=function() { //dzieki querySelector pobieramy z <div id="form"> wszystkie przyciski
		let name = document.querySelector('input[name=name]').value
		let lname = document.querySelector('input[name=lastname]').value
		let email = document.getElementsByName('email')[0].value
		let content = document.getElementsByName('content')[0].value //tworzenie zmiennych dla formularza, które będą zwracane do php
		var sex ='';
		document.querySelectorAll('input[name=sex]').forEach((v) => {
			if (v.checked) 
				sex=v.value ////dzięki tej pętli sprawdzamy która z opcji została wybrana a potem zwrócona jako wartość
		})
		
		/*let name = `<p>${document.querySelector('input[name=name]').value}</p>`
		//let name = `<p>${document.getElementsByName('name')[0].value}</p>`
		let lname = `<p>${document.querySelector('input[name=lastname]').value}</p>`
		//let lname = `<p>${document.getElementsByName('lastname')[0].value}</p>`
		let email = `<p>${document.getElementsByName('email')[0].value}</p>`
		let content = `<p>${document.getElementsByName('content')[0].value}</p>`
		var sex ='';
		document.querySelectorAll('input[name=sex]').forEach((v) => {
			if (v.checked) 
				sex=`<p>${v.value}</p>`
		})*/
		/*document.getElementsByName('sex').forEach((v) => {
			if (v.checked) 
				sex=`<p>${v.value}</p>`
		})*/
		//let sex = `<p>${document.getElementsByName('sex')[2].value}</p>`
		
		//document.querySelector('#output').innerHTML=name+lname+email+content+sex
		post=`name=${name}&lastname=${lname}&email=${email}&content=${content}&sex=${sex}`
		getDocument('./php/main.php','kontakt',post,'#output')
		//Odwolujemy sie do php i wartosci takie jak (imie, nazwisko, email, plec) sa odsylane do php
	}
}
